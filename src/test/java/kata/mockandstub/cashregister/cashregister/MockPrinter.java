package kata.mockandstub.cashregister.cashregister;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MockPrinter extends Printer {
    private Boolean wasCalled;
    private String printThis;

    @Override
    public void print(String printThis) {
        this.wasCalled = true;
        this.printThis = printThis;
    }

    public void verifyThatPrintWasCalled() {
        assertThat("the method print() should be called", wasCalled, is(true));
    }

    public void verifyThatPrintWasCalledWith(String printThis) {
        verifyThatPrintWasCalled();
        assertThat("the method print() should be called with " + printThis, this.printThis, is(printThis));
    }
}
