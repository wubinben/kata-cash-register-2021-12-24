package kata.mockandstub.cashregister.cashregister;

import org.junit.jupiter.api.Test;

import javax.annotation.processing.AbstractProcessor;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CashRegisterTests {

	@Test
	void should_print() {
		Item[] items = new Item[] {
				new Item("Item 1", 10),
				new Item("Item 2", 20)
		};
		Purchase purchase = new Purchase(items);
		MockPrinter mockPrinter = new MockPrinter();
		CashRegister cashRegister = new CashRegister(mockPrinter);

		cashRegister.process(purchase);

		mockPrinter.verifyThatPrintWasCalled();
	}

	@Test
	void should_print_a_stub_purchase() {
		MockPrinter mockPrinter = new MockPrinter();
		StubPurchase stubPurchase = new StubPurchase("purchase as string");
		CashRegister cashRegister = new CashRegister(mockPrinter);

		cashRegister.process(stubPurchase);

		mockPrinter.verifyThatPrintWasCalledWith("purchase as string");
	}

	@Test
	void should_print_a_stub_purchase_using_mockito() {
		Printer mockPrinter = mock(Printer.class);
		Purchase mockPurchase = mock(Purchase.class);
		when(mockPurchase.asString()).thenReturn("purchase as string");
		CashRegister cashRegister = new CashRegister(mockPrinter);

		cashRegister.process(mockPurchase);

		verify(mockPrinter, times(1)).print("purchase as string");
	}

	@Test
	void should_print_a_stub_purchase_using_mockito_bdd() {
		Printer mockPrinter = mock(Printer.class);
		Purchase mockPurchase = mock(Purchase.class);
		given(mockPurchase.asString()).willReturn("purchase as string");
		CashRegister cashRegister = new CashRegister(mockPrinter);

		cashRegister.process(mockPurchase);

		then(mockPrinter).should(times(1)).print("purchase as string");
	}
}
